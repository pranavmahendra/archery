using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {
    private const string SCORE_TEXT = "Score: ";
    private const string PLAYER_TEXT = "Name: ";
    private const string ARROW_COUNT_TEXT = "Arrow Left: ";
    private const string GAME_STATUS_HANDLER_TEXT = "Switching Sides: ";

    [SerializeField] private TextMeshProUGUI scoreLabel;
    [SerializeField] private TextMeshProUGUI playerLabel;
    [SerializeField] private TextMeshProUGUI arrowCountLabel;
    [SerializeField] private TextMeshProUGUI gameStatusLabel;
    [SerializeField] private Button restartBtn;

    private float _countdownTimer = 3;
    private bool _isActivated = false;
    private string _activePlayerName;
    private uint _activePlayerScore;
    private byte _activePlayerArrowCount;

    private void Awake()
    {
        gameStatusLabel.enabled = false;
    }
    
    private void OnEnable()
    {
        GameManager.Instance.PlayerManager.OnCharacterSwitch += OnCharacterChange;
        GameManager.Instance.PlayerManager.OnShootArrow += LabelsTracker;
    }

    void Start()
    {
        restartBtn.onClick.AddListener(RestartGame);
        SetUI(_activePlayerName, _activePlayerScore, _activePlayerArrowCount);
    }

    private void Update()
    {
        GameStatusHandler();
    }

    private void OnDisable()
    {
        GameManager.Instance.PlayerManager.OnCharacterSwitch -= OnCharacterChange;
        GameManager.Instance.PlayerManager.OnShootArrow -= LabelsTracker;
    }

    private void SetUI(string Name, uint Score, byte count)
    {
        this._activePlayerName = Name;
        this._activePlayerScore = Score;
        this._activePlayerArrowCount = count;

        playerLabel.text = PLAYER_TEXT + _activePlayerName;
        scoreLabel.text = SCORE_TEXT + _activePlayerScore;
        arrowCountLabel.text = ARROW_COUNT_TEXT + _activePlayerArrowCount;
    }

    private void OnCharacterChange(PlayerData prevPlayer, PlayerData activePlayer)
    {
        SetUI(activePlayer.Name, activePlayer.Score, activePlayer.ArrowCount);
        DeclareWinner(prevPlayer, activePlayer);
    }


    private void LabelsTracker(uint score, byte arrowCount)
    {
        SetUI(_activePlayerName, score, arrowCount);
        gameStatusLabel.enabled = true;
        _isActivated = true;
    }


    private void GameStatusHandler()
    {
        if ( _activePlayerArrowCount > 0 ) {
            if ( _isActivated && _countdownTimer >= 0 ) {
                gameStatusLabel.text = GAME_STATUS_HANDLER_TEXT + _countdownTimer.ToString("0");
                _countdownTimer -= Time.deltaTime;
            }
            else {
                _isActivated = false;
                _countdownTimer = 3;
                gameStatusLabel.enabled = false;
            }
        }
    }

    private void DeclareWinner(PlayerData prevPlayer, PlayerData activePlayer)
    {
        if ( _activePlayerArrowCount == 0 ) {
            Cursor.lockState = CursorLockMode.None;
            gameStatusLabel.enabled = true;
            restartBtn.gameObject.SetActive(true);

            if ( prevPlayer.Score > activePlayer.Score ) {
                gameStatusLabel.text = prevPlayer.Name + " has won the round.";
            }
            else if ( activePlayer.Score > prevPlayer.Score ) {
                gameStatusLabel.text = activePlayer.Name + " has won the round.";
            }
        }
    }

    private void RestartGame()
    {
        GameManager.Instance.RestartGame();
    }
}