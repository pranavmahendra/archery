using System.Collections;
using UnityEngine;


public class ArrowDestroy : MonoBehaviour {

    private WaitForSeconds _waitTime;

    private void Awake()
    {
        _waitTime = new WaitForSeconds(2);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if ( collision.gameObject.GetComponent<ArrowBehaviourComponent>() ) {

            GameObject missedArrow = collision.gameObject;
            StartCoroutine(DestroyMissedArrow(missedArrow));
        }
    }

    public IEnumerator DestroyMissedArrow(GameObject mesh)
    {
        yield return _waitTime;
        Destroy(mesh);
    }
}
