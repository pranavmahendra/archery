using UnityEngine;

public class ArrowBehaviourComponent : MonoBehaviour {
    private const float PI_FACTOR = 1 / Mathf.PI;
    private const float DEGREE_CONVERSION = 180;

    private Rigidbody _arrowRb;
    private BoxCollider _arrowCollider;

    private float _shootForce = 1500f;
    private float _xVel;
    private float _yVel;
    private float _zVel;
    private float _combinedVel;
    private float _fallAngle;

    public float ShootForce { get { return _shootForce; } set { _shootForce = value; } }

    private void Awake()
    {
        _arrowRb = GetComponent<Rigidbody>();
        _arrowCollider = GetComponent<BoxCollider>();
    }

    private void OnEnable()
    {
        ApplyForce();
    }

    private void FixedUpdate()
    {
        CalculateSpin();
    }

    private void ApplyForce()
    {
        _arrowRb.AddRelativeForce(Vector3.back * _shootForce);
    }

    private void CalculateSpin()
    {
        _xVel = _arrowRb.velocity.x;
        _yVel = _arrowRb.velocity.y;
        _zVel = _arrowRb.velocity.z;
        _combinedVel = Mathf.Sqrt(_xVel * _xVel + _zVel * _zVel);
        _fallAngle = Mathf.Atan2(_yVel, _combinedVel) * DEGREE_CONVERSION * PI_FACTOR;

        this.transform.eulerAngles = new Vector3(_fallAngle, this.transform.eulerAngles.y, this.transform.eulerAngles.z);
    }

    private void OnCollisionEnter(Collision collision)
    {
        FreezeArrow();
    }

    public void FreezeArrow()
    {
        _arrowRb.isKinematic = true;
        _arrowRb.useGravity = false;
        _arrowRb.velocity = Vector3.zero;
        _arrowCollider.enabled = false;
        _arrowRb.transform.parent = null;
    }
}
