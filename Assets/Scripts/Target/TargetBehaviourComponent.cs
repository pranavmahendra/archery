using System.Collections;
using UnityEngine;
using TMPro;

public class TargetBehaviourComponent : MonoBehaviour
{
    public delegate void OnScoreChange(uint point);

    [SerializeField] private TextMeshProUGUI targetScoreCard;

    public event OnScoreChange RewardPoints;

    private WaitForSeconds _waitTime;

    private void Awake()
    {
        _waitTime = new WaitForSeconds(0.5f);
    }

    public IEnumerator Score(byte score)
    {
        targetScoreCard.enabled = true;
        UpdateScore(score);
        yield return _waitTime;
        targetScoreCard.enabled = false;
    }

    private void UpdateScore(byte score)
    {
        targetScoreCard.text = score.ToString();
        RewardPoints(score);
    }

}
