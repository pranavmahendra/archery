using UnityEngine;

public class ColliderData : MonoBehaviour {

    [SerializeField] private byte point;

    private TargetBehaviourComponent targetBehaviourComponent;

    private void Awake()
    {
        targetBehaviourComponent = GetComponentInParent<TargetBehaviourComponent>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if ( collision.gameObject.GetComponent<ArrowBehaviourComponent>() ) {
            StartCoroutine(targetBehaviourComponent.Score(point));
        }
    }
}
