using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;

public class NetworkLauncher : MonoBehaviourPunCallbacks
{
    [SerializeField] private GameObject connectionSuccessfulPanel;
    [SerializeField] private GameSettings gameSettings;
    [SerializeField] private Button connectBtn;

    private void Awake()
    {
        connectionSuccessfulPanel.gameObject.SetActive(false);
        connectBtn.onClick.AddListener(MakeConnection);
    }

    private void Start()
    {
        PhotonNetwork.NickName = gameSettings.Nickname;
        PhotonNetwork.GameVersion = gameSettings.GameVersion;
    }

    private void MakeConnection()
    {
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("Connected To Server");
        Debug.Log("Server name is: " + PhotonNetwork.LocalPlayer.NickName);
        connectionSuccessfulPanel.gameObject.SetActive(true);
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.Log("Disconnected from Server.Reason: " + cause.ToString());
    }

}
