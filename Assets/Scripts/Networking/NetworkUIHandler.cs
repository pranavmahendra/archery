using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using TMPro;

public class NetworkUIHandler : MonoBehaviourPunCallbacks {

    [SerializeField] private TMP_InputField createRoomTF;
    [SerializeField] private TMP_InputField joinRoomTF;
    [SerializeField] private Button createBtn;
    [SerializeField] private Button joinBtn;

    private void Start()
    {
        createBtn.onClick.AddListener(CreateRoom);
        joinBtn.onClick.AddListener(JoinRoom);
    }

    public void JoinRoom()
    {
        PhotonNetwork.JoinRoom(joinRoomTF.text, null);
    }

    public void CreateRoom()
    {
        Debug.Log($"Creating Room");
        PhotonNetwork.CreateRoom(createRoomTF.text, new RoomOptions { MaxPlayers = 2 }, null);
    }

    public override void OnJoinedRoom()
    {
        Debug.Log($"Room Joined Success");
        PhotonNetwork.LoadLevel(1);
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log($"Room Failed {returnCode} Message {message}.");
    }
}
