using UnityEngine;

[CreateAssetMenu(menuName = "Manager/GameSettings")]
public class GameSettings : ScriptableObject
{
    [SerializeField] private string _gameVersion = "0.1";

    public string GameVersion => _gameVersion;

    [SerializeField] private string _nickName = "Player";

    public string Nickname {
        get {
            int value = Random.Range(0, 999);
            return _nickName + value.ToString();
        }
    }
}
