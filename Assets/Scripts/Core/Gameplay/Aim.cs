using UnityEngine;
using System;
using System.Collections;

public class Aim : MonoBehaviour {
    private const string MOUSE_X = "Mouse X";
    private const string MOUSE_Y = "Mouse Y";
    private const int FIRST_FINGER = 0;

    [SerializeField] private float ySpeed = 1f;
    [SerializeField] private float yMinLimit;
    [SerializeField] private float yMaxLimit;

    private float _sensitivity = 10f;
    private float _maxYAngle = 80f;
    private Vector2 _currentRotation;
    private bool _isActivated = false;
    private float _mouseX;
    private float _mouseY;
    private Vector2 _firstpoint;
    private Vector2 _secondpoint;
    private float _xAngTemp;
    private float _xAngle;
    private float _yAngTemp;
    private float _yAngle;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        this.transform.rotation = Quaternion.Euler(Vector3.zero);
    }

    void Update()
    {
        TouchAimControl();

        if ( _mouseX > 0 && !_isActivated ) {
            _isActivated = true;
        }
    }

    private void TouchAimControl()
    {
        if ( Input.touchCount > 0 ) {
            if ( Input.GetTouch(0).phase == TouchPhase.Began ) {
                _firstpoint = Input.GetTouch(0).position;
                _xAngTemp = _xAngle;
                _yAngTemp = _yAngle;
            }

            if ( Input.GetTouch(0).phase == TouchPhase.Moved ) {
                _secondpoint = Input.GetTouch(0).position;

                _yAngle = _yAngTemp - (_secondpoint.y - _firstpoint.y) * 90.0f / Screen.height;
                float swipeSpeed = Input.touches[0].deltaPosition.magnitude / Input.touches[FIRST_FINGER].deltaTime;
                _yAngle -= Input.touches[0].deltaPosition.y * ySpeed * Time.deltaTime * swipeSpeed * 0.00001f;

                _yAngle = Mathf.Clamp(_yAngle, yMinLimit, yMaxLimit);
                if ( _yAngle > 90 && _yAngle < 270 )
                    _xAngle = _xAngTemp - (_secondpoint.x - _firstpoint.x) * 180.0f / Screen.width;
                else
                    _xAngle = _xAngTemp + (_secondpoint.x - _firstpoint.x) * 180.0f / Screen.width;
                if ( _xAngle <= 0 )
                    _xAngle += 360;
                if ( _xAngle > 360 )
                    _xAngle -= 360;
                this.transform.rotation = Quaternion.Euler(_yAngle, _xAngle, 0.0f);
            }
        }
    }

    private void MouseAimControl()
    {
        _mouseX = Input.GetAxis(MOUSE_X);
        _mouseY = Input.GetAxis(MOUSE_Y);

        if ( _isActivated ) {
            _currentRotation.x += _mouseX * _sensitivity;
            _currentRotation.y -= _mouseY * _sensitivity;

            _currentRotation.x = Mathf.Repeat(_currentRotation.x, 360);
            _currentRotation.y = Mathf.Clamp(_currentRotation.y, -_maxYAngle, _maxYAngle);

            this.transform.rotation = Quaternion.Euler(_currentRotation.y, _currentRotation.x, 0);
        }
    }
}
