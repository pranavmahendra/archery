using UnityEngine;

public class ShootArrow : MonoBehaviour {
    private const int LEFT_MOUSE_BUTTON = 0;
    private const float PULL_AMOUNT_FACTOR = 0.01f;

    [SerializeField] private GameObject arrowPrefab;
    [SerializeField] private Transform arrowParent;
    [SerializeField] private float pullSpeed = 100f;

    private PlayerManager _playerManager;
    private GameObject _newArrow;
    private bool _arrowloaded = false;
    private float _pullAmt;
    private SkinnedMeshRenderer _arrowSkin;
    private Rigidbody _arrowRB;
    private SkinnedMeshRenderer _bowSkin;
    private ArrowBehaviourComponent _arrowBehaviour;

    private void Awake()
    {
        _bowSkin = this.GetComponent<SkinnedMeshRenderer>();
        _playerManager = GameManager.Instance.PlayerManager;
    }

    private void Start()
    {
        SpawnArrow();
    }

    private void Update()
    {
        if ( _playerManager.PlayerTurnCompleted ) {
            InputHandler();
        }
    }

    private void FixedUpdate()
    {
        if ( _playerManager.PlayerTurnCompleted ) {
            Shoot();
        }
    }

    private void SpawnArrow()
    {
        if ( _playerManager.ActivePlayer.ArrowCount > 0 && !_arrowloaded ) {
            _arrowloaded = true;
            _newArrow = Instantiate(arrowPrefab, arrowParent.transform.position, arrowParent.transform.rotation) as GameObject;
            _newArrow.transform.parent = arrowParent;

            _arrowSkin = _newArrow.GetComponent<SkinnedMeshRenderer>();
            _arrowRB = _newArrow.GetComponent<Rigidbody>();
            _arrowBehaviour = _newArrow.GetComponent<ArrowBehaviourComponent>();
        }
    }

    private void Shoot()
    {
        if ( _playerManager.ActivePlayer.ArrowCount > 0 && _newArrow != null ) {
            if ( _pullAmt > 100 ) {
                _pullAmt = 100;
            }

            _arrowSkin.SetBlendShapeWeight(0, _pullAmt);
            _bowSkin.SetBlendShapeWeight(0, _pullAmt);
        }
    }

    private void InputHandler()
    {
        if ( Input.touchCount > 0 ) {
            Touch fingerTouch = Input.GetTouch(0);

            if ( _playerManager.ActivePlayer.ArrowCount > 0 && _newArrow != null ) {
                if ( fingerTouch.phase == TouchPhase.Moved || fingerTouch.phase == TouchPhase.Stationary ) {
                    _pullAmt += Time.deltaTime * pullSpeed;
                }

                if ( fingerTouch.phase == TouchPhase.Ended ) {
                    _arrowloaded = false;
                    _arrowRB.isKinematic = false;
                    _arrowBehaviour.ShootForce = _arrowBehaviour.ShootForce * ((_pullAmt * PULL_AMOUNT_FACTOR) + 0.05f);
                    _playerManager.DeductArrow();
                    _arrowBehaviour.enabled = true;
                    _pullAmt = 0;
                }
            }
            if ( fingerTouch.phase == TouchPhase.Began ) {
                SpawnArrow();
            }
        }
    }
}
