public interface IGameManager
{
    public PlayerManager PlayerManager { get; }
    public TargetBehaviourComponent TargetManager { get; }
    public void RestartGame();
}
