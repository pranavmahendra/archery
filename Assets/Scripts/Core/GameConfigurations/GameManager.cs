using UnityEngine.SceneManagement;
using UnityEngine;

public class GameManager : MonoBehaviour, IGameManager {
    private static GameManager _instance;
    public static IGameManager Instance => _instance;

    private Scene _activeScene;
    private int _activeSceneIndex;

    [SerializeField] private PlayerManager playerMgr;
    public PlayerManager PlayerManager { get { return playerMgr; } }

    [SerializeField] private TargetBehaviourComponent targetMgr;
    public TargetBehaviourComponent TargetManager { get { return targetMgr; } }

    private void Awake()
    {
        if ( ValidateInstance() )
            return;

        _activeScene = SceneManager.GetActiveScene();
        _activeSceneIndex = _activeScene.buildIndex;
    }

    private bool ValidateInstance()
    {
        if ( _instance == null ) {
            _instance = this;
            return false;
        }
        Destroy(gameObject);
        return true;
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(_activeSceneIndex);
    }
}

// Instantiate the Bow Prefab, with parent set to MainCamera.