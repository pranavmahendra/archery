public struct PlayerData 
{
    private const byte DEFAULT_ARROW_COUNT = 3;
    private const uint DEFAULT_SCORE = 0;

    private string _name;
    public string Name => _name;
    
    private uint _score;
    public uint Score => _score;

    private byte _arrowCount;
    public byte ArrowCount => _arrowCount;

    public PlayerData(string Name)
    {
        this._name = Name;
        this._score = DEFAULT_SCORE;
        this._arrowCount = DEFAULT_ARROW_COUNT;
    }

    public void SetScore(uint Score)
    {
        _score += Score;
    }

    public void SetArrowCount()
    {
        _arrowCount--;
    }
}
