using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {
    private const byte MAX_PLAYABLE_CHARACTERS = 2;

    #region events
    public delegate void OnCharacterChange(PlayerData PrevChar, PlayerData NextChar);
    public delegate void OnArrowShot(uint score, byte arrowCount);
    #endregion

    private PlayerData _activePlayer;
    private PlayerData _nullPlayerData;
    private List<PlayerData> _playerCollections = new List<PlayerData>();
    private byte _selectedCharacterIndex = 0;
    private bool _playerTurnCompleted = true;
    private WaitForSeconds _waitAfterTurn;

    public PlayerData ActivePlayer { get { return _activePlayer; } }
    public bool PlayerTurnCompleted { get { return _playerTurnCompleted; } }

    public event OnCharacterChange OnCharacterSwitch = delegate { };
    public event OnArrowShot OnShootArrow = delegate { };

    private void Awake()
    {
        _waitAfterTurn = new WaitForSeconds(3);
        _nullPlayerData = new PlayerData();
    }

    private void Start()
    {
        Initialization();
    }

    private void OnEnable()
    {
        GameManager.Instance.TargetManager.RewardPoints += GiveScore; //TargetManager is invoking the event, which is broadcasting score.
    }

    private void OnDisable()
    {
        GameManager.Instance.TargetManager.RewardPoints -= GiveScore;
    }

    private void Initialization()
    {
        for ( int i = 0; i < MAX_PLAYABLE_CHARACTERS; i++ ) {
            int PlayerIndex = i + 1;
            _playerCollections.Add(new PlayerData("Player " + PlayerIndex.ToString()));
        }
        _activePlayer = _playerCollections[_selectedCharacterIndex];
        OnCharacterSwitch(_nullPlayerData, _activePlayer);

#if UNITY_EDITOR
        Debug.Log($"Active Player is: {_activePlayer.Name}");
#endif
    }


    public void CharacterSwitchHandler()
    {
        byte previousIndex = _selectedCharacterIndex;

        // Update the values of current active player in the list.
        _playerCollections[previousIndex] = _activePlayer;

        // Update the Character Index.
        switch ( _selectedCharacterIndex ) {
            case 0:
                _selectedCharacterIndex++;
                break;

            case 1:
                _selectedCharacterIndex--;
                break;
            default:
                break;
        }

        // Update the active player with new character index.
        _activePlayer = _playerCollections[_selectedCharacterIndex];

        // Invoke CharacterSwitch Event.
        OnCharacterSwitch(_playerCollections[previousIndex], _activePlayer);
        
        _playerTurnCompleted = true;
    }

    public void GiveScore(uint point)
    {
        _activePlayer.SetScore(point);
        OnShootArrow(_activePlayer.Score, _activePlayer.ArrowCount);
    }

    public void DeductArrow()
    {
        _playerTurnCompleted = false;
        _activePlayer.SetArrowCount();
        OnShootArrow(_activePlayer.Score, _activePlayer.ArrowCount);

        StartCoroutine(InitateCharacterSwitch());
    }

    private IEnumerator InitateCharacterSwitch()
    {
        yield return _waitAfterTurn;
        CharacterSwitchHandler();
    }
}